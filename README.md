# Lab 6 - Fuzz Testing

Student name: <b>Marko Pezer</b><br>
Student email: <b>m.pezer@innopolis.university</b><br>
Student group: <b>BS18-SE-01</b>

## Screenshot

Here is the screenshot of `stryker run` execution:

![image info](./screenshots/sqr_lab6.JPG)