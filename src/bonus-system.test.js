import { calculateBonuses } from "./bonus-system.js";
const assert = require("assert");

// Testing Bonus System
describe('Testing Bonus System', () => {

    // Free
    test('Testing Standard Bonuses', (done) => {
        expect(calculateBonuses('Standard', 9999)).toBeCloseTo(0.05, 10);
        expect(calculateBonuses('Standard', 10000)).toBeCloseTo(0.075, 10);
        expect(calculateBonuses('Standard', 50000)).toBeCloseTo(0.1, 10);
        expect(calculateBonuses('Standard', 100000)).toBeCloseTo(0.125, 10);
        done();
    });

    // Premium
    test('Testing Premium Bonuses', (done) => {
        expect(calculateBonuses('Premium', 9999)).toBeCloseTo(0.1, 10);
        expect(calculateBonuses('Premium', 10000)).toBeCloseTo(0.15, 10);
        expect(calculateBonuses('Premium', 50000)).toBeCloseTo(0.2, 10);
        expect(calculateBonuses('Premium', 100000)).toBeCloseTo(0.25, 10);
        done();
    });

    // Diamond
    test('Testing Diamond Bonuses', (done) => {
        expect(calculateBonuses('Diamond', 9999)).toBeCloseTo(0.2, 10);
        expect(calculateBonuses('Diamond', 10000)).toBeCloseTo(0.3, 10);
        expect(calculateBonuses('Diamond', 50000)).toBeCloseTo(0.4, 10);
        expect(calculateBonuses('Diamond', 100000)).toBeCloseTo(0.5, 10);
        done();
    });

    // Invalid 
    test('Testing Invalid Program', (done) => {
        expect(calculateBonuses('Random', 9999)).toEqual(0);
        expect(calculateBonuses('Random', 10000)).toEqual(0);
        expect(calculateBonuses('Random', 50000)).toEqual(0);
        expect(calculateBonuses('Random', 100000)).toEqual(0);
        done();
    });
});